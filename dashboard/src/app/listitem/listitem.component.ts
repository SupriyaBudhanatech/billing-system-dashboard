import { Component, OnInit,ViewChild } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-listitem',
  templateUrl: './listitem.component.html',
  styleUrls: ['./listitem.component.css']
})
export class ListitemComponent implements OnInit {


  displayedColumns: string[] = ['position','item', 'amount'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);

  displayedColumns1: string[] = ['position','type', 'number','date','quantity','price','status'];
  dataSource1 = new MatTableDataSource<PeriodicElement1>(ELEMENT_DATA1);


  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
  }
}

export interface PeriodicElement {
  item: string;
  position: number;

  amount:number;
}

export interface PeriodicElement1 {
  position: number;

  type: string;
  number: number;

  date: number;
  quantity:number;
  price:number;
  status: string;


}



const ELEMENT_DATA1: PeriodicElement1[] = [
  {position: 1, type: 'sale', date: 7/7/2020 , number:1, quantity:120, price:1200,status:'paid'}
];


const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, item: 'Led Lamp', amount: 1.0079}
];


