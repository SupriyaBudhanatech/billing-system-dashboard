import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BackupdriveComponent } from './backupdrive.component';

describe('BackupdriveComponent', () => {
  let component: BackupdriveComponent;
  let fixture: ComponentFixture<BackupdriveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BackupdriveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BackupdriveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
