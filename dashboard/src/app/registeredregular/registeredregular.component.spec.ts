import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisteredregularComponent } from './registeredregular.component';

describe('RegisteredregularComponent', () => {
  let component: RegisteredregularComponent;
  let fixture: ComponentFixture<RegisteredregularComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisteredregularComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisteredregularComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
