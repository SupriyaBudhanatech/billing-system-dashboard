import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisteredcompoComponent } from './registeredcompo.component';

describe('RegisteredcompoComponent', () => {
  let component: RegisteredcompoComponent;
  let fixture: ComponentFixture<RegisteredcompoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisteredcompoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisteredcompoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
