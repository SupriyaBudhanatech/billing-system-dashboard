import { Component, OnInit } from '@angular/core';

interface Party{
  value:string;
  viewValue: string;

} 

interface State{
  value:string;
  viewValue: string;

}
@Component({
  selector: 'app-addparty',
  templateUrl: './addparty.component.html',
  styleUrls: ['./addparty.component.css']
})
export class AddpartyComponent  {
PartyType:Party[]=[
  {value:'Unregistered/consumer0', viewValue:'Unregistered/consumer'},

  {value:'Registered Business Regular1', viewValue:'Registered Business Regular'},
  {value:'Registered Business Composition2', viewValue:'Registered Business Composition'},

];
 States:State[]=[
 
  {value:'AP|Andhra Pradesh0', viewValue: 'AP|Andhra Pradesh'},
 {value: 'AR|Arunachal Pradesh1', viewValue:'AR|Arunachal Pradesh'},
 {value : 'AS|Assam2' , viewValue:'AS|Assam'},
  {value :'BR|Bihar3',viewValue:'BR|Bihar'},
  {value:'CT|Chhattisgarh3', viewValue:'Chhattisgarh'},
 {value:'GA|Gova4',viewValue:'GA|Gova'},
 {value:' GJ|Gujarat5',viewValue:'GJ|Gujarat'},
 {value:'HR|Haryana6', viewValue:'   HR|Haryana '},
 {value:'  HP|Himachal Pradesh7',  viewValue:' HP|Himachal Pradesh'},
 {value:'JH|Jharkhand8',viewValue:'JH|Jharkhand'},
 {value:'KA|Karnataka10',viewValue:'  KA|Karnataka'},
 {value:'  KL|Kerala11',viewValue:'  KL|Kerala'},
 {value:'MP|Madhya Pradesh12',viewValue:'  MP|Madhya Pradesh'},
 {value:'  MN|Manipur13',viewValue:'  MN|Manipur '},
 {value:'ML|Meghalaya14',viewValue:'  ML|Meghalaya'},
 {value:'MZ|Mizoram15',viewValue:'  MZ|Mizoram'},

 {value:'  NL|Nagaland16',viewValue:'  NL|Nagaland'},
 {value:'  OR|Odisha17',viewValue:'  OR|Odisha'},
 {value:'  SK|Sikkim18 ',viewValue:'  SK|Sikkim'},
 {value:'  TN|Tamil Nadu19',viewValue:'  TN|Tamil Nadu'},
 {value:'  TG|Telangana20',viewValue:'  TG|Telangana'},
 {value:'  TR|Tripura21',viewValue:'  TR|Tripura'},
 {value:'  UT|Uttarakhand22',viewValue:'  UT|Uttarakhand'},
 {value:'  RJ|Rajasthan23',viewValue:'  RJ|Rajasthan'},
 {value:'  UP|Uttar Pradesh24',viewValue:'  UP|Uttar Pradesh'},
 {value:'  PB|Punjab25',viewValue:'  PB|Punjab'},
 {value:'  DL|Delhi26',viewValue:'  DL|Delhi'},
 

 {value:'  WB|West Bengal27',viewValue:'  WB|West Bengal'},
 {value:'  AN|Andaman and Nicobar Islands28',viewValue:'  AN|Andaman and Nicobar Islands'},
 {value:'  CH|Chandigarh29 ',viewValue:'  CH|Chandigarh'},
 {value:'  DN|Dadra and Nagar Haveli30',viewValue:'  DN|Dadra and Nagar Haveli'},
 {value:'  DD|Daman and Diu31',viewValue:'  DD|Daman and Diu'},
 {value:'  LD|Lakshadweep32',viewValue:'  LD|Lakshadweep'},
 {value:'  PY|Puducherry33',viewValue:'  PY|Puducherry '}
  

 ];
}
