import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchasebillsComponent } from './purchasebills.component';

describe('PurchasebillsComponent', () => {
  let component: PurchasebillsComponent;
  let fixture: ComponentFixture<PurchasebillsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchasebillsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchasebillsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
