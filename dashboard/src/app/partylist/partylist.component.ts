import { Component, OnInit,ViewChild } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';


@Component({
  selector: 'app-partylist',
  templateUrl: './partylist.component.html',
  styleUrls: ['./partylist.component.css']
})
export class PartylistComponent implements OnInit {


  displayedColumns: string[] = ['position','party', 'amount'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);

  displayedColumns1: string[] = ['position','type', 'number','date','total','balance','due','status'];
  dataSource1 = new MatTableDataSource<PeriodicElement1>(ELEMENT_DATA1);


  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
  }
}

export interface PeriodicElement {
  party: string;
  position: number;

  amount:number;
}

export interface PeriodicElement1 {
  position: number;

  type: string;
  number: number;

  date: number;
  total:number;
  balance:number;
  due:number;
  status: string;


}



const ELEMENT_DATA1: PeriodicElement1[] = [
  {position: 1, type: 'Credit note', date: 7/7/2020 , number:1, total:45, balance:4522, due:7/7/2020 ,  status:'paid'}
];


const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, party: 'Hydrogen', amount: 1.0079}
];




