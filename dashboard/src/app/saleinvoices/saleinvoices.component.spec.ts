import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaleinvoicesComponent } from './saleinvoices.component';

describe('SaleinvoicesComponent', () => {
  let component: SaleinvoicesComponent;
  let fixture: ComponentFixture<SaleinvoicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaleinvoicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaleinvoicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
