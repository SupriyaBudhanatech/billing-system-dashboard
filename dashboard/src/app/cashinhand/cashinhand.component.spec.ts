import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CashinhandComponent } from './cashinhand.component';

describe('CashinhandComponent', () => {
  let component: CashinhandComponent;
  let fixture: ComponentFixture<CashinhandComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CashinhandComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CashinhandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
