import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BackupcomputerComponent } from './backupcomputer.component';

describe('BackupcomputerComponent', () => {
  let component: BackupcomputerComponent;
  let fixture: ComponentFixture<BackupcomputerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BackupcomputerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BackupcomputerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
