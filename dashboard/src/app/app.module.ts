import { BrowserModule } from '@angular/platform-browser';
import{FormsModule,ReactiveFormsModule}  from '@angular/forms'
import { NgModule } from '@angular/core';
import { MaterialModule } from './material.module'; // material module imported
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddpartyComponent } from './addparty/addparty.component';
import { AdditemComponent } from './additem/additem.component';
import { SaleinvoicesComponent } from './saleinvoices/saleinvoices.component';
import { EstimationComponent } from './estimation/estimation.component';
import { PaymentinComponent } from './paymentin/paymentin.component';
import { SaleorderComponent } from './saleorder/saleorder.component';
import { DeliverychallanComponent } from './deliverychallan/deliverychallan.component';
import { SalereturnComponent } from './salereturn/salereturn.component';
import { ExpensesComponent } from './expenses/expenses.component';
import { BankaccountComponent } from './bankaccount/bankaccount.component';
import { ChequeComponent } from './cheque/cheque.component';
import { LoanaccountComponent } from './loanaccount/loanaccount.component';
import { ReportComponent } from './report/report.component';
import { BackupcomputerComponent } from './backupcomputer/backupcomputer.component';
import { BackupdriveComponent } from './backupdrive/backupdrive.component';
import { RestorebackupComponent } from './restorebackup/restorebackup.component';
import { PurchasebillsComponent } from './purchasebills/purchasebills.component';
import { PaymentoutComponent } from './paymentout/paymentout.component';
import { PurchaseorderComponent } from './purchaseorder/purchaseorder.component';
import { PurchasereturnComponent } from './purchasereturn/purchasereturn.component';
import { UtilitiesComponent } from './utilities/utilities.component';
import { ViewitemComponent } from './viewitem/viewitem.component';
import { ListitemComponent } from './listitem/listitem.component';
import { AddsaleComponent } from './addsale/addsale.component';
import { EditsaleComponent } from './editsale/editsale.component';
import { CashinhandComponent } from './cashinhand/cashinhand.component';
import { SidemenuComponent } from './sidemenu/sidemenu.component';
import { UnregisterComponent } from './unregister/unregister.component';
import { RegisteredregularComponent } from './registeredregular/registeredregular.component';
import { RegisteredcompoComponent } from './registeredcompo/registeredcompo.component';
import { PartylistComponent } from './partylist/partylist.component';


@NgModule({
  declarations: [
    AppComponent,
    AddpartyComponent,
    AdditemComponent,
    SaleinvoicesComponent,
    EstimationComponent,
    PaymentinComponent,
    SaleorderComponent,
    DeliverychallanComponent,
    SalereturnComponent,
    ExpensesComponent,
    BankaccountComponent,
    FormsModule,
    ReactiveFormsModule,
    
    ChequeComponent,
    LoanaccountComponent,
    ReportComponent,
    BackupcomputerComponent,
    BackupdriveComponent,
    RestorebackupComponent,
    PurchasebillsComponent,
    PaymentoutComponent,
    PurchaseorderComponent,
    PurchasereturnComponent,
    UtilitiesComponent,
    ViewitemComponent,
    ListitemComponent,
    AddsaleComponent,
    EditsaleComponent,
    CashinhandComponent,
    SidemenuComponent,
    UnregisterComponent,
    RegisteredregularComponent,
    RegisteredcompoComponent,
    PartylistComponent,
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule // MAteria module added

    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
